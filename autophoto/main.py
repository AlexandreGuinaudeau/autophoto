from http.server import HTTPServer

from server.configuration import CONFIG
from server.server import PhotoRequestHandler


server_config = CONFIG.conf["server"]
ADDRESS = server_config["address"]
PORT = server_config["port"]
httpd = HTTPServer((ADDRESS, PORT), PhotoRequestHandler)
print("Server starting at %s:%s" % (ADDRESS, PORT))
httpd.serve_forever()
