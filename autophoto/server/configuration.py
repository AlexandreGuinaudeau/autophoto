#  Copyright 2019 Alexandre Guinaudeau. All rights reserved.
import os
import yaml


class _Configuration:
    def __init__(self):
        config_path = os.path.realpath(os.path.join(__file__, "..", "..", "..", "config.yml"))
        with open(config_path, 'r') as stream:
            self.conf = yaml.safe_load(stream)

CONFIG = _Configuration()
