#  Copyright 2019 Alexandre Guinaudeau. All rights reserved.
import os
import platform
import subprocess
import re
import numpy as np
import time
from datetime import datetime

import shutil
from sklearn.cluster import KMeans
from sklearn.metrics import pairwise_distances_argmin_min
from .image import Image, IMAGE_EXTENSIONS
from .storage import STORAGE


def safe_listdir(path):
    try:
        return os.listdir(path)
    except (PermissionError, OSError):
        return []


def images_in_folder(root_dir, depth=3):
    """
    Returns the list of images in this folder and subfolders

    Parameters
    ==========
    root_dir: string, root directory
    depth: number of recursions in sub-directories

    Returns
    =======
    img_files: list of paths of images
    is_exact: whether there were still folders left to iterate on
    """
    regex = re.compile(".*\.(%s)" % "|".join(IMAGE_EXTENSIONS), re.IGNORECASE)
    all_files = [os.path.join(root_dir, f) for f in safe_listdir(root_dir)]
    folders = [f for f in all_files if os.path.isdir(f)]
    img_files = [f for f in all_files if regex.match(f)]
    for _ in range(depth):
        new_files = [os.path.join(folder, f) for folder in folders for f in safe_listdir(folder)]
        folders = [f for f in new_files if os.path.isdir(f)]
        new_img_files = [f for f in new_files if regex.match(f)]
        img_files.extend(new_img_files)
    return img_files, len(folders) == 0


class Album:
    def __init__(self, frontend_data_dir, album):
        STORAGE.set_album(album)
        self.album = album
        self.frontend_data_dir = frontend_data_dir
        self.album_dir = os.path.join(frontend_data_dir, album)
        self.export_dir = os.path.realpath(os.path.join(__file__, "..", "albums", self.album))

    def create(self, path):
        os.mkdir(self.album_dir)
        name = os.path.basename(path)
        os.symlink(path, os.path.join(self.album_dir, name), True)

    def enrich(self, path):
        name = os.path.basename(path)
        new_name = name
        i = 1
        while os.path.exists(os.path.join(self.album_dir, new_name)):
            new_name = "%s_%i" % (name, i)
            i += 1
        os.symlink(path, os.path.join(self.album_dir, new_name), True)

    def rename(self, name):
        new_album_dir = os.path.join(self.frontend_data_dir, name)
        if os.path.exists(new_album_dir):
            raise FileExistsError(new_album_dir)
        shutil.move(self.album_dir, new_album_dir)
        STORAGE.set_album(self.album)
        src_storage = STORAGE.path
        STORAGE.set_album(name)
        dst_storage = STORAGE.path
        shutil.move(src_storage, dst_storage)
        self.album_dir = new_album_dir
        self.album = name

    def delete(self):
        for name in safe_listdir(self.album_dir):
            path = os.path.join(self.album_dir, name)
            if os.path.islink(path):
                continue
            if platform.system() == "Windows" and os.path.isdir(path):
                # Windows considers symlinks to dirs as dirs
                continue
            raise AssertionError("Unexpected file %s: it is not a symlink" % path)
        # All good, remove all links
        for name in safe_listdir(self.album_dir):
            path = os.path.join(self.album_dir, name)
            if os.path.islink(path):
                os.unlink(path)
            if platform.system() == "Windows" and os.path.isdir(path):
                os.rmdir(path)
        os.rmdir(self.album_dir)
        STORAGE.set_album(self.album)
        os.remove(STORAGE.path)

    @staticmethod
    def get_unix_time(s):
        if s is None:
            return None
        return int(time.mktime(datetime.strptime(s, "%Y:%m:%d %H:%M:%S").timetuple()))

    def suggest_photos(self, photos, n):
        # Run k-means
        photo_statuses = {
            os.path.join(self.album_dir, photo["name"]): photo["status"]
            for photo in photos
        }
        images = [Image(img_path)
                  for img_path in images_in_folder(self.album_dir, depth=10)[0]
                  if photo_statuses[img_path] != "IGNORED"]
        if len(images) <= 1:
            return
        if n == 0:
            n = 1
        if n >= len(images):
            n = len(images) - 1

        times = [self.get_unix_time(img.exif().get("DateTime", None)) for img in images]
        min_time = min(times) or 0
        max_time = max(times) or 0
        time_weight = 5000
        times = [int(time_weight*(t-min_time)/(max_time-min_time))
                 if t is not None
                 else time_weight
                 for t in times]
        features = [list(img.features().values()) + [[times[i]]] for i, img in enumerate(images)]
        x = np.array([np.concatenate(f).flatten() for f in features])
        kmeans = KMeans(n_clusters=n, random_state=0).fit(x)

        # Ignore clusters that already have a selected image
        selected_clusters = [kmeans.labels_[i]
                             for i, img in enumerate(images)
                             if photo_statuses[img.img_path] == "SELECTED"]
        other_clusters = [c for i, c in enumerate(kmeans.cluster_centers_) if i not in selected_clusters]
        closest, _ = pairwise_distances_argmin_min(other_clusters, x)

        # Reset suggestions
        [img.set_status("DEFAULT") for img in images if photo_statuses[img.img_path] == "SUGGESTED"]
        for c in closest:
            images[c].set_status("SUGGESTED")
        return closest

    def get_all_photos(self):
        img_paths, is_exact = images_in_folder(self.album_dir, depth=10)
        photos = []
        for img_path in img_paths:
            img = Image(img_path)
            photos.append({
                "name": img_path[len(self.album_dir)+1:],
                "album": self.album,
                "status": img.status(),
                "tags": img.tags(),
                "exif": img.exif()
            })

        return sorted(photos, key=lambda p: p["exif"].get("DateTime", None) or p["name"])

    def get_photos_with_features(self):
        img_paths, is_exact = images_in_folder(self.album_dir, depth=10)
        photos = []
        for img_path in img_paths:
            img = Image(img_path)
            photos.append({
                "name": img_path[len(self.album_dir)+1:],
                "album": self.album,
                "status": img.status(),
                "tags": img.tags(),
                "exif": img.exif(),
                "features": img.features()
            })

        return sorted(photos, key=lambda p: p["exif"].get("DateTime", None) or p["name"])

    def save(self, photos):
        with STORAGE.lock:
            data = STORAGE.data
            for photo in photos:
                data[os.path.join(self.album_dir, photo["name"])]["status"] = photo["status"]
                data[os.path.join(self.album_dir, photo["name"])]["tags"] = photo.get("tags", [])
            STORAGE._store_data(data)
    
    def list_exports(self):
        return safe_listdir(self.export_dir)
    
    def delete_export(self, export_name):
        output = os.path.join(self.export_dir, export_name)
        if os.path.exists(output):
            shutil.rmtree(output)
        return output

    def export(self, photos, export_name):
        output = os.path.join(self.export_dir, export_name)
        if os.path.exists(output):
            shutil.rmtree(output)
        os.makedirs(output)
        imgs = {}
        for photo in photos:
            img = Image(os.path.join(self.album_dir, photo["name"]))
            img_name = os.path.basename(img.img_path)
            i = 0
            while img_name in imgs.keys():
                root, ext = os.path.splitext(os.path.basename(img.img_path))
                img_name = "%s_%i%s" % (root, i, ext)
                i += 1
            imgs[img_name] = img.img_path
        for img_name, img_path in imgs.items():
            shutil.copy(img_path, os.path.join(output, img_name))
        return output

    def open_with_filesystem(self, path):
        path = os.path.realpath(os.path.join(self.album_dir, path))
        print(path)
        if platform.system() == 'Darwin':       # macOS
            subprocess.call(('open', path))
        elif platform.system() == 'Windows':    # Windows
            os.startfile(path)
        else:                                   # linux variants
            subprocess.call(('xdg-open', path))
