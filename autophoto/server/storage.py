#  Copyright 2019 Alexandre Guinaudeau. All rights reserved.
import json
import os
import threading


class _Storage:
    def __init__(self):
        self.data_path = os.path.realpath(os.path.join(__file__, "..", "data"))
        self.album = None
        self.path = None
        self.data = None
        self.lock = threading.RLock()

    def _store_data(self, data):
        with self.lock:
            with open(self.path, "w") as out_f:
                json.dump(data, out_f)

    def _load_data(self):
        with self.lock:
            with open(self.path, "r") as in_f:
                return json.load(in_f)

    def set_album(self, album):
        with self.lock:
            self.album = album
            self.path = os.path.join(self.data_path, "%s.json" % self.album)
            if not os.path.isfile(self.path):
                self._store_data({})
            self.data = self._load_data()

    def memoize(self, keys, override=False):
        with self.lock:
            if isinstance(keys, str):
                keys = [keys]

            def set_value(d, value, sub_keys):
                if len(sub_keys) == 0:
                    return value
                d[sub_keys[0]] = set_value(d.get(sub_keys[0], {}), value, sub_keys[1:])
                return d

            def wrapper(f):
                def inner(self_, *args):
                    all_keys = [self_.img_path] + keys
                    if override:
                        value = f(self_, *args)
                        self.data = set_value(self.data, value, all_keys)
                        self._store_data(self.data)
                        return value

                    d = self.data
                    for k in all_keys:
                        d = d.get(k, None)
                        if d is None:
                            value = f(self_, *args)
                            self.data = set_value(self.data, value, all_keys)
                            self._store_data(self.data)
                            return value
                    return d
                return inner
            return wrapper

STORAGE = _Storage()
