#  Copyright 2019 Alexandre Guinaudeau. All rights reserved.
import os
from datetime import datetime
import cv2 as cv
import PIL.Image
import numpy as np
from PIL.ExifTags import TAGS, GPSTAGS
from .storage import STORAGE

_TAGS_r = dict(((v, k) for k, v in TAGS.items()))
_GPSTAGS_r = dict(((v, k) for k, v in GPSTAGS.items()))
IMAGE_EXTENSIONS = ['bmp', 'gif', 'ico', 'ief', 'jpe', 'jpeg', 'jpg', 'pbm', 'pct', 'pgm', 'pic', 'pict', 'png', 'pnm', 'ppm', 'ras', 'rgb', 'svg', 'tif', 'tiff', 'xbm', 'xpm', 'xwd']


class Image:
    def __init__(self, img_path):
        self.img_path = img_path
        self._cv_img = None
        self._pil_img = None
        self._cv_img_hsv = None

    @property
    def cv_img(self):
        if self._cv_img is None:
            self._cv_img = cv.imread(self.img_path)
            if self._cv_img is None:
                print("Failed to open image %s with cv2" % self.img_path)
        return self._cv_img

    @property
    def cv_img_hsv(self):
        if self._cv_img_hsv is None:
            self._cv_img_hsv = cv.cvtColor(self.cv_img, cv.COLOR_BGR2HSV)
        return self._cv_img_hsv

    @property
    def pil_img(self):
        if self._pil_img is None:
            self._pil_img = PIL.Image.open(self.img_path)
            if self._pil_img is None:
                raise Exception("Failed to open image %s with PIL" % self.img_path)
        return self._pil_img

    @staticmethod
    def maybe_decode(value):
        if isinstance(value, bytes):
            return str(value)
        return value

    @STORAGE.memoize("status")
    def status(self):
        return "DEFAULT"

    @STORAGE.memoize("status", override=True)
    def set_status(self, status):
        return status
    
    @STORAGE.memoize("tags")
    def tags(self):
        return []

    def exif(self):
        to_ignore = ["MakerNote", "UserComment", "GPSInfo"]
        try:
            exifd = self.pil_img._getexif() or {}
        except AttributeError:
            exifd = {}
        keys = [k for k in list(exifd.keys()) if k in TAGS]
        exifd = {TAGS[k]: self.maybe_decode(exifd[k]) for k in keys}
        for k in to_ignore:
            exifd.pop(k, None)
        return exifd

    @staticmethod
    def date_time(exif, file):
        if "DateTime" in exif.keys():
            date = datetime.strptime(exif["DateTime"], "YYYY-MM-DD HH:mm:ss")
        else:
            date = datetime.fromtimestamp(os.path.getctime(file))
        return date.strftime("%Y-%m-%d %H:%M:%S")

    def features(self):
        return {
            "histogram_hue_saturation": self.histogram_hue_saturation(),
            "histogram_hue": self.histogram_hue(),
            "histogram_saturation": self.histogram_saturation()
        }

    @STORAGE.memoize(["features", "histogram_hue"])
    def histogram_hue(self):
        # hue varies from 0 to 179
        return self._histogram([0], [12], [0, 180])

    @STORAGE.memoize(["features", "histogram_saturation"])
    def histogram_saturation(self):
        # saturation varies from 0 to 255
        return self._histogram([1], [5], [0, 256])

    @STORAGE.memoize(["features", "histogram_hue_saturation"])
    def histogram_hue_saturation(self):
        return self._histogram([0, 1], [12, 2], [0, 180, 0, 256])

    def _histogram(self, channels, histSize, ranges):
        try:
            # hue varies from 0 to 179, saturation from 0 to 255
            hist = cv.calcHist([self.cv_img_hsv], channels, None, histSize, ranges, accumulate=False)
            cv.normalize(hist, hist, alpha=0, beta=1, norm_type=cv.NORM_MINMAX)
            return [int(1000*x) for x in hist.ravel()]
        except Exception as e:
            print("Exception", e)
            return list(np.zeros(histSize).flatten())
