#  Copyright 2019 Alexandre Guinaudeau. All rights reserved.
import os
import time
import json

from .album import Album, images_in_folder, safe_listdir


def get_method(f):
    def wrapper(self, params):
        start = time.time()
        result = f(self, **params)
        print("GET /%s(%s) took %.2f seconds to execute" %
              (f.__name__, ",".join(["%s: %s" % (k, v) for k, v in dict(params).items()]), time.time() - start))
        return result

    wrapper.__is_get_method__ = True
    return wrapper


def post_method(f):
    def wrapper(self, body, params):
        start = time.time()
        if body:
            result = f(self, json.loads(body.decode("utf-8")), **params)
        else:
            result = f(self, **params)
        print("POST /%s(%s) took %.2f seconds to execute" %
              (f.__name__, ",".join(["%s: %s" % (k, v) for k, v in dict(params).items()]), time.time() - start))
        return result

    wrapper.__is_post_method__ = True
    return wrapper


class PhotoServer:
    def __init__(self):
        self.frontend_data_dir = os.path.realpath(
            os.path.join(__file__, "..", "..", "..", "classeur-photo-react", "packages", "classeur-photo", "src", "data")
        )
        self.album = None

    @get_method
    def albums(self):
        return os.listdir(self.frontend_data_dir)

    @get_method
    def home_dir(self):
        return {"home": os.path.realpath(os.path.expanduser("~")), "separator": os.sep}

    @get_method
    def list_dirs(self, path):
        subfolders = sorted([name for name in safe_listdir(path)
                             if os.path.isdir(os.path.join(path, name)) and not name.startswith(".")])
        resolved_files = [{"folder": folder, "files": images_in_folder(os.path.join(path, folder))}
                          for folder in subfolders]
        current_folder_images = images_in_folder(path, 0)
        return {
            "current": {
                "num_images": len(current_folder_images[0]) + sum([len(r["files"][0]) for r in resolved_files]),
                "is_exact": all([r["files"][1] for r in resolved_files]),
                "folder": path
            },
            "children": [{
                    "folder": r["folder"],
                    "num_images": len(r["files"][0]),
                    "is_exact": r["files"][1]
                } for r in resolved_files]
        }

    @post_method
    def create_album(self, path, album):
        return Album(self.frontend_data_dir, album).create(path)

    @post_method
    def enrich_album(self, path, album):
        return Album(self.frontend_data_dir, album).enrich(path)

    @post_method
    def rename_album(self, album, new_name):
        return Album(self.frontend_data_dir, album).rename(new_name)

    @post_method
    def delete_album(self, album):
        return Album(self.frontend_data_dir, album).delete()
    
    @get_method
    def album_exports(self, album):
        return Album(self.frontend_data_dir, album).list_exports()
    
    @post_method
    def delete_export(self, body):
        return Album(self.frontend_data_dir, body["album"]).delete_export(body["export_name"])

    @post_method
    def export_album(self, body):
        self.album = Album(self.frontend_data_dir, body["album"])
        self.album.save(body["photos"])
        output = self.album.export(body["photos"], body["export_name"])
        return self.album.open_with_filesystem(output)

    @get_method
    def photos(self, album):
        self.album = Album(self.frontend_data_dir, album)
        return self.album.get_all_photos()

    @get_method
    def photo_features(self, album):
        self.album = Album(self.frontend_data_dir, album)
        return self.album.get_photos_with_features()

    @post_method
    def suggest_photos(self, body):
        self.album = Album(self.frontend_data_dir, body["album"])
        self.album.save(body["photos"])
        self.album.suggest_photos(body["photos"], body["n_photos"])
        return self.album.get_all_photos()

    @post_method
    def save(self, body):
        self.album = Album(self.frontend_data_dir, body["album"])
        return self.album.save(body["photos"])

    @post_method
    def open(self, album, path):
        self.album = Album(self.frontend_data_dir, album)
        return self.album.open_with_filesystem(path)
