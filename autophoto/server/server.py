#  Copyright 2019 Alexandre Guinaudeau. All rights reserved.
from http.server import BaseHTTPRequestHandler
import json
import traceback
import urllib.parse
from .photo_server import PhotoServer


class PhotoRequestHandler(BaseHTTPRequestHandler):
    photo_server = PhotoServer()

    @staticmethod
    def cast_to_bytes(input):
        return bytes(json.dumps(input), 'utf-8')

    def parse_path(self):
        endpoint, *query_params = self.path[1:].split("?")
        query_params = "".join(list(*query_params))
        query_params = [qp.split("=") for qp in query_params.split("&")] if query_params else []
        query_params = {key[0]: urllib.parse.unquote(key[1]) for key in query_params}
        return endpoint, query_params

    def do_OPTIONS(self):
        self.send_response(200, "ok")
        self.send_header('Access-Control-Allow-Credentials', 'true')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
        self.send_header("Access-Control-Allow-Headers", "X-Requested-With, Content-type")

    def do_GET(self):
        try:
            endpoint, query_params = self.parse_path()
            if self.path == "/favicon.ico":
                self.send_response(404)
            elif hasattr(self.photo_server, endpoint):
                method = getattr(self.photo_server, endpoint)
                if not method.__is_get_method__:
                    print("%s is not a valid GET method" % endpoint)
                    self.send_response(400)
                    return
                response = method(query_params)
                self.send_response(200, 'ok')
                self.send_header('Access-Control-Allow-Origin', '*')
                self.send_header("Content-type", "application/json")
                self.end_headers()
                self.wfile.write(self.cast_to_bytes(response))
            else:
                print("Unexpected request GET %s" % self.path, endpoint, query_params)
                self.send_response(404)
        except Exception as e:
            print("Unexpected exception:", e)
            traceback.print_exc()
            self.send_response(500)

    def do_POST(self):
        try:
            endpoint, query_params = self.parse_path()
            if hasattr(self.photo_server, endpoint):
                method = getattr(self.photo_server, endpoint)
                if not method.__is_post_method__:
                    print("%s is not a valid POST method" % endpoint)
                    self.send_response(400)
                    return
                content_length = int(self.headers['Content-Length'] or 0)
                body = self.rfile.read(content_length)
                self.send_response(200, 'ok')
                self.send_header('Access-Control-Allow-Origin', '*')
                self.send_header("Content-type", "application/json")
                self.end_headers()
                response = method(body, query_params)
                self.wfile.write(self.cast_to_bytes(response))
            else:
                print("Unexpected request POST %s" % self.path)
                self.send_response(404)
        except Exception as e:
            print("Unexpected exception:", e)
            traceback.print_exc()
            self.send_response(500)
