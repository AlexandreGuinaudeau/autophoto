/*
 * Copyright 2019 Alexandre Guinaudeau. All rights reserved.
 */
const { baseConfig } = require("@blueprintjs/webpack-build-scripts");
const path = require("path");

module.exports = Object.assign({}, baseConfig, {

    devServer: {
        ...baseConfig.devServer,
        watchContentBase: true,
        watchOptions: {
            ...baseConfig.devServer.watchOptions,
            ignored: [
                path.resolve(__dirname, 'dist'),
                path.resolve(__dirname, 'node_modules')
            ]
        }
    },

    entry: {
        core: "./src/index.ts",
    },

    output: {
        filename: "[name].bundle.js",
        path: path.resolve(__dirname, "./dist")
    },
});