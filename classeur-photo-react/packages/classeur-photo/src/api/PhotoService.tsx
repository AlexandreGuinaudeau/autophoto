/*
 * Copyright 2019 Alexandre Guinaudeau. All rights reserved.
 */
import moment, { Moment } from 'moment';
import { PhotoStatus } from '../components/photo_selection/photoSelector';

interface ExifData {
    ImageWidth?: string;
    ImageLength?: string;
    BitsPerSample?: string;
    Compression?: string;
    DateTimeDigitized?: string;
    MaxApertureValue?: string;
    PhotometricInterpretation?: string;
    Threshholding?: string;
    CellWidth?: string;
    CellLenght?: string;
    FillOrder?: string;
    DocumentName?: string;
    ImageDescription?: string;
    Make?: string;
    Model?: string;
    StripOffsets?: string;
    Orientation?: string;
    YCbCrPositioning?: string;
    ReferenceBlackWhite?: string;
    SamplesPerPixel?: string;
    RowsPerStrip?: string;
    StripByteConunts?: string;
    MinSampleValue?: string;
    MaxSampleValue?: string;
    XResolution?: string;
    YResolution?: string;
    PlanarConfiguration?: string;
    PageName?: string;
    FileSource?: string;
    ExifInteroperabilityOffset?: string;
    FreeOffsets?: string;
    FreeByteCounts?: string;
    GrayResponseUnit?: string;
    GrayResponseCurve?: string;
    SpectralSensitivity?: string;
    GPSInfo?: string;
    ISOSpeedRatings?: string;
    ResolutionUnit?: string;
    Interlace?: string;
    TimeZoneOffset?: string;
    SelfTimerMode?: string;
    TransferFunction?: string;
    Saturation?: string;
    CameraOwnerName?: string;
    Software?: string;
    DateTime?: string;
    LensMake?: string;
    JpegIFOffset?: string;
    LensSerialNumber?: string;
    Flash?: string;
    LensSpecification?: string;
    Artist?: string;
    HostComputer?: string;
    FocalLength?: string;
    WhitePoint?: string;
    PrimaryChromaticities?: string;
    ColorMap?: string;
    ExifImageWidth?: string;
    FlashEnergy?: string;
    CustomRendered?: string;
    FlashPixVersion?: string;
    SpatialFrequencyResponse?: string;
    SubjectDistance?: string;
    UserComment?: string;
    Noise?: string;
    ExtraSamples?: string;
    FocalPlaneXResolution?: string;
    CFAPattern?: string;
    FocalPlaneYResolution?: string;
    ExifImageHeight?: string;
    FocalPlaneResolutionUnit?: string;
    ExposureMode?: string;
    LensModel?: string;
    YCbCrCoefficients?: string;
    MeteringMode?: string;
    ExifOffset?: string;
    ExposureBiasValue?: string;
    YCbCrSubSampling?: string;
    DeviceSettingDescription?: string;
    InterColorProfile?: string;
    SubjectLocation?: string;
    SubjectDistanceRange?: string;
    MakerNote?: string;
    RelatedSoundFile?: string;
    WhiteBalance?: string;
    RelatedImageFileFormat?: string;
    SensingMethod?: string;
    ExposureIndex?: string;
    CFARepeatPatternDim?: string;
    BatteryLevel?: string;
    SubsecTime?: string;
    SubsecTimeOriginal?: string;
    SubsecTimeDigitized?: string;
    ImageNumber?: string;
    Copyright?: string;
    ExposureTime?: string;
    JpegIFByteCount?: string;
    FNumber?: string;
    DigitalZoomRatio?: string;
    RelatedImageWidth?: string;
    SceneType?: string;
    ComponentsConfiguration?: string;
    SecurityClassification?: string;
    ShutterSpeedValue?: string;
    FocalLengthIn35mmFilm?: string;
    ImageUniqueID?: string;
    RelatedImageLength?: string;
    ExifVersion?: string;
    Sharpness?: string;
    CompressedBitsPerPixel?: string;
    ExposureProgram?: string;
    Gamma?: string;
    ImageHistory?: string;
    LightSource?: string;
    ApertureValue?: string;
    SceneCaptureType?: string;
    ColorSpace?: string;
    DateTimeOriginal?: string;
    BodySerialNumber?: string;
    BrightnessValue?: string;
    OECF?: string;
    GainControl?: string;
    Contrast?: string;
    NewSubfileType?: string;
    SubfileType?: string;
}

export interface ImageFeatures {
    histogram_hue_saturation: number[];
    histogram_hue: number[];
    histogram_saturation: number[];
}
export const imageFeatures: Array<keyof ImageFeatures> = ["histogram_hue_saturation", "histogram_hue", "histogram_saturation"]

export interface ImageTag {
    category: string;
    value: string;
}

export interface Photo {
    name: string;
    album: string;
    exif?: ExifData;
    features?: ImageFeatures;
    status: PhotoStatus;
    tags?: ImageTag[];
    src?: string;
    date: Moment;
    distance?: number;
}

export interface FileSystem {
    home: string;
    separator: string;
}

export interface ImageFolder {
    folder: string;
    num_images: number;
    is_exact: boolean;
}

export interface ListDirResult {
    current: ImageFolder;
    children: ImageFolder[];
}

export interface IPhotoService {
    getPhotos(): Promise<Photo[]>;
}

export class PhotoService {
    url: string;

    constructor() {
        this.url = "http://localhost:8000"
    }
    
    public getPhotos(album: string): Promise<Photo[]> {
        return this.callApi("/photos", {album: album}, {}, this.enrichPhoto);
    }

    public getPhotoFeatures(album: string): Promise<Photo[]> {
        return this.callApi("/photo_features", {album: album}, {}, this.enrichPhoto);
    }

    public suggestPhotos(album: string, photos: Photo[], n_photos: number): Promise<Photo[]> {
        return this.callApi("/suggest_photos", {}, {
            method: "POST", body: JSON.stringify({album: album, photos: photos, n_photos: n_photos})
        }, this.enrichPhoto);
    }

    public save(album: string, photos: Photo[]): Promise<void> {
        return this.callApi("/save", {}, {
            method: "POST", body: JSON.stringify({album: album, photos: photos})
        });
    }

    public open(album: string, path: string): Promise<void> {
        return this.callApi("/open", {album: album, path: path}, {
            method: "POST"
        });
    }

    public getAlbums(): Promise<string[]> {
        return this.callApi("/albums");
    }

    public getHomeDir(): Promise<FileSystem> {
        return this.callApi("/home_dir");
    }

    public listDirs(path: string): Promise<ListDirResult> {
        return this.callApi("/list_dirs", {path: path});
    }

    public createAlbum(path: string, album: string): Promise<void> {
        return this.callApi("/create_album", {path: path, album: album}, {method:"post"});
    }

    public enrichAlbum(path: string, album: string): Promise<void> {
        return this.callApi("/enrich_album", {path: path, album: album}, {method:"post"});
    }

    public renameAlbum(new_name: string, album: string): Promise<void> {
        return this.callApi("/rename_album", {new_name: new_name, album: album}, {method:"post"});
    }

    public deleteAlbum(album: string): Promise<void> {
        return this.callApi("/delete_album", {album: album}, {method:"post"});
    }

    public listExports(album: string): Promise<string[]> {
        return this.callApi("/album_exports", {album: album});
    }

    public deleteAlbumExport(album: string, export_name: string): Promise<string[]> {
        return this.callApi("/delete_export", {}, {
            method:"post", body: JSON.stringify({album: album, export_name: export_name})
        });
    }

    public export(album: string, photos: Photo[], export_name: string): Promise<void> {
        return this.callApi("/export_album", {}, {
            method: "POST", body: JSON.stringify({album: album, photos: photos, export_name: export_name})
        });
    }

    private callApi(endpoint: string, params?: {[key: string] : any}, init?: RequestInit, mapFunction?: (json: any) => any): Promise<any> {
        if (params) {
            let sep = "?";
            Object.keys(params).forEach(key => {
                endpoint = `${endpoint}${sep}${key}=${encodeURIComponent(params[key])}`
                sep = "&"
            });
        }
        return new Promise((resolve, reject) => {
            fetch(this.url + endpoint, init).then(
                (response: Response) => {
                    response.json().then(json => {
                        const result: any = mapFunction ? json.map(mapFunction) : json;
                        resolve(result);
                    }).catch(e => {
                        reject(e);
                    });
                }
            ).catch(e => {
                reject(e);
            });
        });
    }

    private enrichPhoto(photo: Photo): Photo {
        return Object.assign(photo, {
            src: `./data/${photo.album}/${photo.name}`,
            date: moment(photo.exif.DateTime, "YYYY-MM-DD HH:mm:ss"),
        });
    }
}

