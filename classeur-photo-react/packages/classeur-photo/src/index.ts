/*
 * Copyright 2019 Alexandre Guinaudeau. All rights reserved.
 */
import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/app';

import './app.scss';

ReactDOM.render(
   React.createElement(App),
   document.getElementById('app')
);

