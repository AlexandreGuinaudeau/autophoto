/*
 * Copyright 2019 Alexandre Guinaudeau. All rights reserved.
 */
import { Photo, ImageTag } from '../../api/PhotoService'
import { ExifPhoto } from './exifPhoto';
import { Component, createRef, RefObject, } from "react";
import React from 'react';
import { Card, Elevation, NonIdealState, Popover, Menu, MenuItem, Position, Tag, Intent } from '@blueprintjs/core';
import { TimeSort, SimilaritySort } from '../album';
import { MultiSelectTag } from '../tag_selection/tagSelector';
const classNames = require('classnames');

export const PhotoStatus = {
    IGNORED: "IGNORED",
    DEFAULT: "DEFAULT",
    SUGGESTED: "SUGGESTED",
    SELECTED: "SELECTED",
}
export declare type PhotoStatus = typeof PhotoStatus[keyof typeof PhotoStatus];

export interface PhotoSelectorProps {
    photos: Photo[];
    tags: ImageTag[];
    visible: boolean[];
    selected: number;
    selectTagInput: boolean;
    sort: TimeSort | SimilaritySort;
    categoryColor: { [category: string]: string };
    onSelectPhoto: (index: number, autoScroll: boolean) => void;
    onChangeStatus: (status: PhotoStatus) => void;
    getStatusIcon: (status: PhotoStatus) => JSX.Element;
    onCreateTag: (tag: ImageTag) => void;
    onSetPhotoTags: (tags: ImageTag[]) => void;
}

export class PhotoSelector extends Component<PhotoSelectorProps> {
    selectedCard: HTMLDivElement | null;
    ref: RefObject<any>;
    maxNameLength = 30;

    constructor(props: PhotoSelectorProps) {
        super(props)
        this.selectedCard = null;
        this.ref = createRef();
    }

    public componentDidUpdate() {
        if (this.selectedCard !== null && this.ref.current !== null) {
            const parent = this.getScrollParent(this.ref.current, true);
            parent.scrollTo( 0, this.selectedCard.offsetTop - ( window.innerHeight / 2 ) );
            if (this.props.selectTagInput) {
                const inputChild = this.getInputChild(this.selectedCard);
                if (inputChild != null) {
                    inputChild.focus();
                }
            }
        }
    }

    public render() {
        const { photos, selected, visible } = this.props;
        if (photos.length == 0) {
            return <NonIdealState title={"There are no photos in the current album"}/>
        }
        return (
        <div ref={this.ref}>
            {
                photos.map((photo, index) => this.renderCard(photo, index, visible[index], selected))
            }
        </div>);
    }

    private renderCard(photo: Photo, index: number, visible: boolean, selectedIndex: number) {
        return (
        <div key={index} ref={(el) => {
            if (index == selectedIndex) {
                this.selectedCard = el;
            }
        }}>
            <Card 
                className={classNames("img-card", !visible && "hidden", index == selectedIndex && "selected")}
                interactive={true} 
                elevation={Elevation.ONE}
                onClick={() => this.props.onSelectPhoto(index, false)}
            >
                {this.renderTags(photo.tags || [], index == selectedIndex)}
                {this.renderStatus(photo.status)}
                <div className={"img-metadata"}>
                    <strong className={"wrap-text"}>{
                        photo.name.length > this.maxNameLength ?
                            "..." + photo.name.substr(photo.name.length - this.maxNameLength) :
                            photo.name
                    }</strong>
                    <p>{photo.date.format("DD MMM YYYY")}</p>
                </div>
                <ExifPhoto className={"thumbnail"} photo={photo}/>
            </Card>
        </div>);
    }

    private renderStatus(status: PhotoStatus) {
        return (
        <div className={"fixed-position"}>
            <div className={"status-icon"}>
                <Popover position={Position.LEFT_TOP}>
                    {this.props.getStatusIcon(status)}
                    <Menu>
                        {
                            Object.keys(PhotoStatus).map((otherStatus: PhotoStatus) => 
                            <MenuItem 
                                icon={this.props.getStatusIcon(otherStatus)} 
                                text={otherStatus}
                                disabled={status == otherStatus}
                                onClick={() => this.props.onChangeStatus(otherStatus)}
                                key={otherStatus}
                            />
                            )
                        }
                    </Menu>
                </Popover>
            </div>
        </div>);
    }

    private renderTags(tags: ImageTag[], selected: boolean) {
        return (
        <div className={"fixed-position tags-container"}>
            <div className={"tag-icon"}>
            {
                selected ?
                <MultiSelectTag
                    items={this.props.tags}
                    categoryColor={this.props.categoryColor}
                    selectedItems={tags}
                    allowCreate={true}
                    createItem={this.props.onCreateTag}
                    onSetSelectedItems={this.props.onSetPhotoTags}
                    placeholder={"Search Tags..."}
                /> :
                <div>
                    {
                        tags.length ? tags.map(tag =>
                            <Tag
                                intent={Intent.NONE}
                                round={true}
                                key={`${tag.category}:${tag.value}`}
                                style={{backgroundColor: this.props.categoryColor[tag.category]}}
                            >
                                {tag.value}
                            </Tag>
                        ) : undefined
                    }
                </div>
            }
            </div>
        </div>);
    }

    private getScrollParent = (element: Element, includeHidden: boolean) => {
        var style = getComputedStyle(element);
        var excludeStaticParent = style.position === "absolute";
        var overflowRegex = includeHidden ? /(auto|scroll|hidden)/ : /(auto|scroll)/;
    
        if (style.position === "fixed") return document.body;
        for (var parent = element; (parent = parent.parentElement);) {
            style = getComputedStyle(parent);
            if (excludeStaticParent && style.position === "static") {
                continue;
            }
            if (overflowRegex.test(style.overflow + style.overflowY + style.overflowX)) return parent;
        }
    
        return document.body;
    }

    private getInputChild(node: any): HTMLElement | null {
        // Hack to get the input across children
        if (node.className == "bp3-tag-input-values") {
            return node.lastChild as HTMLElement;
        }
        if (node.hasChildNodes() && node.firstChild.focus !== undefined) {
            return this.getInputChild(node.firstChild);
        }
        return null;
    }
}