/*
 * Copyright 2019 Alexandre Guinaudeau. All rights reserved.
 */
import { Photo, PhotoService } from '../../api/PhotoService'
import { Component, } from "react";
import React from 'react';
import { Button, Intent, InputGroup, ButtonGroup, Alert } from '@blueprintjs/core';

export interface AlbumExportManagerProps {
    photos: Photo[];
    album: string;
    photoService: PhotoService;
}

export interface AlbumExportManagerState {
    export_name: string;
    albums: string[];
    exportToDelete: string | null;
}

export class AlbumExportManager extends Component<AlbumExportManagerProps, AlbumExportManagerState> {
    
    constructor (props: AlbumExportManagerProps) {
        super(props);

        this.state = {
            export_name: "",
            albums: [],
            exportToDelete: null
        }

        props.photoService.listExports(props.album).then(
            (albums) => this.setState({albums})
        )
    }

    public render() {
        const { photos } = this.props;
        const { export_name, albums, exportToDelete } = this.state;
        const isUpdate = albums.indexOf(export_name) >= 0;
        return (
        <div>
            <Alert
                    cancelButtonText="Cancel"
                    confirmButtonText={`Delete ${exportToDelete}`}
                    icon="trash"
                    intent={Intent.DANGER}
                    isOpen={exportToDelete != null}
                    onCancel={this.onCancel}
                    onClose={this.onCancel}
                    onConfirm={this.onDeleteAlbumExport}
            >
                <p>
                    Are you sure you want to delete <b>{exportToDelete}</b>?
                    You will be not able to restore it later.
                </p>
            </Alert>
            {albums.map(this.renderAlbum)}
            <InputGroup
                leftIcon="edit"
                defaultValue={export_name}
                onChange={this.handleChangeExportName}
            />
            <Button
                disabled={export_name == ""}
                fill={true}
                intent={isUpdate ? Intent.WARNING : Intent.SUCCESS}
                text={`${isUpdate ? "Update album" : "Create a new album"} ${export_name} with ${photos.length} selected photos`}
                onClick={this.onExportAlbum}
            />
        </div>);
    }

    private renderAlbum = (album: string) => {
        return <ButtonGroup key={album} minimal={true} fill={true}>
            <Button
                icon={"folder-close"}
                text={album}
                onClick={() => this.handleSetExportName(album)}
                className={"display-inline-block"}
            />
            <Button
                icon={"trash"}
                intent={Intent.DANGER}
                className={"flex-unset"}
                onClick={() => this.setState({exportToDelete: album})}
            />
        </ButtonGroup>
    }

    private handleSetExportName = (export_name: string) => {
        this.setState({export_name})
    }

    private handleChangeExportName = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({export_name: event.target.value})
    }

    private onExportAlbum = () => {
        this.props.photoService.export(this.props.album, this.props.photos, this.state.export_name);
    }

    private onCancel = () => {
        this.setState({exportToDelete: null});
    }

    private onDeleteAlbumExport = () => {
        const { exportToDelete } = this.state;
        const { album } = this.props;
        this.props.photoService.deleteAlbumExport(album, exportToDelete).then(
            () => this.setState(prevState => ({
                albums: prevState.albums.filter(album => album != exportToDelete),
                exportToDelete: null
            }))
        );
    }
}