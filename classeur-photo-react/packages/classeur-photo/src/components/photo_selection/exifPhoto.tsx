/*
 * Copyright 2019 Alexandre Guinaudeau. All rights reserved.
 */
import React, { Component } from 'react';
import { Photo } from "../../api/PhotoService";
import classNames from 'classnames';

export interface ExifPhotoProps {
    photo: Photo;
    className?: string;
}

export class ExifPhoto extends Component<ExifPhotoProps> {
    public render () {
        const { photo, className } = this.props;
        const style : any = {}
        let vertical = true;
        if (photo.exif.Orientation) {
            const orientation = parseInt(photo.exif.Orientation);
            style["transform"] = "";

            if ([2, 4, 5, 7].includes(orientation)) {
                style["transform"] += "scaleX(-1)"
            }
            switch (Math.floor((orientation-1) / 2)) {
                case 0:
                    break;
                case 1:
                    style["transform"] += " rotate(180deg)";
                    break;
                case 2:
                    style["transform"] = " rotate(90deg)";
                    vertical = false;
                    break;
                case 3:
                    style["transform"] = " rotate(270deg)";
                    vertical = false;
                    break;
                default:
                    break;
            }
        }
        return <img className={classNames(className, vertical? "vertical": "horizontal")} src={photo.src} style={style}/>;
    }
}