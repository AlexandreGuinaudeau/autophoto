/*
 * Copyright 2019 Alexandre Guinaudeau. All rights reserved.
 */
import { Photo } from '../../api/PhotoService'
import { Component } from "react";
import React from 'react';
import { NonIdealState } from '@blueprintjs/core';

export interface PhotoMetadataProps {
    photo: Photo;
}

export class PhotoMetadata extends Component<PhotoMetadataProps, {}> {
    public render() {
        const { photo } = this.props;
        const keys = Object.keys(photo.exif);
        if (keys.length == 0) {
            return <NonIdealState title={"The image has no EXIF metadata"}/>
        }
        return (
        <div>
            {
                keys.map(key => this.renderMetadata(key, (photo.exif as any)[key]))
            }
        </div>);
    }

    private renderMetadata(key: string, value: any) {
        return (
        <div className={"exif-metadata"} key={key}>
                <strong className={"metadata-key"}>{key}</strong>
                <span className={"metadata-value"}>{JSON.stringify(value)}</span>
        </div>
        );
    }
}