// Adapted from https://github.com/palantir/blueprint/blob/develop/packages/docs-app/src/examples/select-examples/tags.tsx

import { MenuItem } from "@blueprintjs/core";
import { ItemPredicate, ItemRenderer } from "@blueprintjs/select";
import * as React from "react";
import { ImageTag } from '../../api/PhotoService';


export const renderTag: ItemRenderer<ImageTag> = (tag: ImageTag, { handleClick, modifiers, query }) => {
    if (!modifiers.matchesPredicate) {
        return null;
    }
    const text = `${tag.category}:${tag.value}`;
    return (
        <MenuItem
            active={modifiers.active}
            disabled={modifiers.disabled}
            label={tag.category}
            key={text}
            onClick={handleClick}
            text={highlightText(text, query)}
        />
    );
};

export const renderCreateTagOption = (
    query: string,
    active: boolean,
    handleClick: React.MouseEventHandler<HTMLElement>,
) => (
    <MenuItem
        icon="add"
        text={`Create "${query}"`}
        active={active}
        disabled={(query.match(/:/g) || []).length != 1}
        onClick={handleClick}
        shouldDismissPopover={false}
    />
);

export const filterTag: ItemPredicate<ImageTag> = (query, tag, _index, exactMatch) => {
    const text = `${tag.category}:${tag.value}`;
    const normalizedTag = tag.value.toLowerCase();
    const normalizedQuery = query.toLowerCase();

    if (exactMatch) {
        return normalizedTag === normalizedQuery;
    } else {
        return `${text} ${normalizedTag}`.indexOf(normalizedQuery) >= 0;
    }
};

function highlightText(text: string, query: string) {
    let lastIndex = 0;
    const words = query
        .split(/\s+/)
        .filter(word => word.length > 0)
        .map(escapeRegExpChars);
    if (words.length === 0) {
        return [text];
    }
    const regexp = new RegExp(words.join("|"), "gi");
    const tokens: React.ReactNode[] = [];
    while (true) {
        const match = regexp.exec(text);
        if (!match) {
            break;
        }
        const length = match[0].length;
        const before = text.slice(lastIndex, regexp.lastIndex - length);
        if (before.length > 0) {
            tokens.push(before);
        }
        lastIndex = regexp.lastIndex;
        tokens.push(<strong key={lastIndex}>{match[0]}</strong>);
    }
    const rest = text.slice(lastIndex);
    if (rest.length > 0) {
        tokens.push(rest);
    }
    return tokens;
}

function escapeRegExpChars(text: string) {
    return text.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

export function createTag(text: string): ImageTag {
    const splitText = text.split(":");
    return {
        category: splitText[0],
        value: splitText[1]
    };
}

function isValidTag(object: any) : boolean {
    return object.value && object.category;
}

export function areTagsEqual(tagA: ImageTag, tagB: ImageTag) {
    if (!isValidTag(tagB)) {
        return false;
    }
    return tagA.value.toLowerCase() === tagB.value.toLowerCase()
        && tagA.category.toLowerCase() === tagB.category.toLowerCase();
}

export function doesTagEqualQuery(tag: ImageTag, query: string) {
    return tag.value.toLowerCase() === query.toLowerCase();
}

export function arrayContainsTag(tags: ImageTag[], tagToFind: ImageTag): boolean {
    return tags.some((tag: ImageTag) => tag.value === tagToFind.value
        && tag.category == tagToFind.category);
}

export function addTagToArray(tags: ImageTag[], tagToAdd: ImageTag) {
    return [...tags, tagToAdd];
}

export function deleteTagFromArray(tags: ImageTag[], tagToDelete: ImageTag) {
    return tags.filter(tag => !areTagsEqual(tag, tagToDelete));
}

export function maybeAddCreatedTagToArrays(
    items: ImageTag[],
    tag: ImageTag,
): { items: ImageTag[] } {
    const isNewlyCreatedItem = !arrayContainsTag(items, tag);
    return {
        // Add a created tag to `items` so that the tag can be deselected.
        items: isNewlyCreatedItem ? addTagToArray(items, tag) : items,
    };
}
