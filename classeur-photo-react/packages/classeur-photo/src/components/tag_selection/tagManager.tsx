/*
 * Copyright 2019 Alexandre Guinaudeau. All rights reserved.
 */
import React, { Component, CSSProperties } from 'react';
import _ from 'lodash';
import { Classes, Button, Intent, Icon } from '@blueprintjs/core';
import { ImageTag } from '../../api/PhotoService';


export interface TagManagerProps {
    tags: ImageTag[];
    categoryColor: { [category: string]: string };
    onDeleteTag(tag: ImageTag): void;
    onEditTag(oldTag: ImageTag, newTag: ImageTag): void;
    onSetCategoryColor(category: string, color: string): void;
}

export interface TagManagerState {
    editValue: string;
    isEditing: boolean;
    editColor: string;
}


export interface ImageTagEdit {
    category?: string;
    value?: string;
}

export class TagManager extends Component<TagManagerProps, TagManagerState> {
    public state = {
        editValue: "",
        editColor: "",
        isEditing: false
    }
    private style: CSSProperties = {
        "width": 200,
    }

    public render() {
        return <div className={"margin-auto"}>
            <h3>
                Manage Tags
            </h3>
            {this.props.tags.map(this.renderTag)}
            <h3>
                Manage Categories
            </h3>
            {_.uniq(this.props.tags.map(t => t.category)).map(this.renderCategory)}
        </div>
    }

    private renderTag = (tag: ImageTag, index: number) => {
        const { editValue } = this.state;
        return <div className={"margin-auto"} key={`${tag.category}-${tag.value}`}>
            <input
                className={Classes.INPUT}
                type="text"
                placeholder="Category"
                dir="auto"
                defaultValue={tag.category}
                onChange={this.setEditValue}
                onBlur={() => this.onEditTag(tag, {category: editValue})}
                style={this.style}
            />
            <input
                className={Classes.INPUT}
                type="text"
                placeholder="Value"
                dir="auto"
                defaultValue={tag.value}
                onChange={this.setEditValue}
                onBlur={() => this.onEditTag(tag, {value: editValue})}
                style={this.style}
            />
            <Button
                icon="cross"
                minimal={true}
                intent={Intent.DANGER}
                onClick={() => this.deleteTag(index)}
            />
        </div>
    }

    private renderCategory = (category: string) => {
        return <div className={"margin-auto"} key={category}>
            <input
                readOnly={true}
                className={Classes.INPUT}
                type="text"
                placeholder="Category"
                dir="auto"
                defaultValue={category}
                style={this.style}
            />
            <input
                className={Classes.INPUT}
                type="text"
                placeholder="Color"
                dir="auto"
                defaultValue={this.props.categoryColor[category] || ""}
                onChange={this.setColorValue}
                onBlur={() => this.onEditColor(category)}
                style={this.style}
            />
            <Icon
                icon="symbol-square"
                style={{color: this.props.categoryColor[category]}}
            />
        </div>
    }

    private setEditValue = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({editValue: event.target.value, isEditing: true});
    }

    private onEditTag = (tag: ImageTag, edit: ImageTagEdit) => {
        if (!this.state.isEditing) {
            return;
        }
        if (edit.category) {
            const newTag: ImageTag = {
                category: edit.category,
                value: tag.value
            }
            this.props.onEditTag(tag, newTag);
        } else if (edit.value) {
            const newTag: ImageTag = {
                category: tag.category,
                value: edit.value
            }
            this.props.onEditTag(tag, newTag);
        }
        this.setState({isEditing: false})
    }

    private deleteTag = (index: number) => {
        this.props.onDeleteTag(this.props.tags[index]);
    }

    private setColorValue = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({editColor: event.target.value, isEditing: true});
    }

    private onEditColor = (category: string) => {
        if (!this.state.isEditing) {
            return;
        }
        this.setState({isEditing: false});
        this.props.onSetCategoryColor(category, this.state.editColor);
    }
}