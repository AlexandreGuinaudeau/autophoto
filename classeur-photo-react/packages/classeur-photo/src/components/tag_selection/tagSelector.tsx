/*
 * Adapted from https://github.com/palantir/blueprint/blob/develop/packages/docs-app/src/examples/select-examples/multiSelectTag.tsx
 */

import * as React from "react";

import { Button, Intent, MenuItem, Tag } from "@blueprintjs/core";
import { ItemRenderer, MultiSelect } from "@blueprintjs/select";
import {
    areTagsEqual,
    arrayContainsTag,
    createTag,
    maybeAddCreatedTagToArrays,
    renderCreateTagOption,
    filterTag,
    renderTag,
} from "./tags";
import { ImageTag } from '../../api/PhotoService';

const TagMultiSelect = MultiSelect.ofType<ImageTag>();

const INTENTS = [Intent.NONE, Intent.PRIMARY, Intent.SUCCESS, Intent.DANGER, Intent.WARNING];

export interface IMultiSelectTagProps {
    items: ImageTag[];
    categoryColor: { [category: string]: string };
    selectedItems: ImageTag[];
    allowCreate: boolean;
    createItem?: (tag: ImageTag) => void;
    onSetSelectedItems: (tags: ImageTag[]) => void;
    className?: string;
    placeholder?: string;
}

export interface IMultiSelectTagState {
    allowCreate: boolean;
    fill: boolean;
    tags: ImageTag[];
    intent: boolean;
    items: ImageTag[];
    openOnKeyDown: boolean;
    popoverMinimal: boolean;
    resetOnSelect: boolean;
    tagMinimal: boolean;
}

export class MultiSelectTag extends React.PureComponent<IMultiSelectTagProps, IMultiSelectTagState> {

    constructor(props: IMultiSelectTagProps) {
        super(props);

        this.state = {
            allowCreate: props.allowCreate,
            fill: true,
            tags: props.selectedItems,
            intent: false,
            items: props.items,
            openOnKeyDown: true,
            popoverMinimal: true,
            resetOnSelect: true,
            tagMinimal: false,
        };
    }

    public componentWillReceiveProps(props: IMultiSelectTagProps) {
        this.setState({items: props.items});
    }

    public render() {
        const { allowCreate, tags, tagMinimal, popoverMinimal, ...flags } = this.state;
        const getTagProps = (_value: string, index: number) => ({
            style: {backgroundColor: this.props.categoryColor[tags[index].category]},
            minimal: tagMinimal,
            round: true
        });

        const tagSelectProps = {
            itemPredicate: filterTag,
            itemRenderer: renderTag,
            items: this.props.items,
            className: this.props.className,
            placeholder: this.props.placeholder
        };

        const maybeCreateNewItemFromQuery = allowCreate ? createTag : undefined;
        const maybeCreateNewItemRenderer = allowCreate ? renderCreateTagOption : null;

        const clearButton =
            tags.length > 0 ? <Button icon="cross" minimal={true} onClick={this.handleClear} /> : undefined;

        return (
            <TagMultiSelect
                {...tagSelectProps}
                {...flags}
                createNewItemFromQuery={maybeCreateNewItemFromQuery}
                createNewItemRenderer={maybeCreateNewItemRenderer}
                itemRenderer={this.renderTag}
                itemsEqual={areTagsEqual}
                // we may customize the default tagSelectProps.items by
                // adding newly created items to the list, so pass our own
                items={this.state.items}
                noResults={<MenuItem disabled={true} text="No results." />}
                onItemSelect={this.handleTagSelect}
                onItemsPaste={this.handleTagsPaste}
                popoverProps={{ minimal: popoverMinimal }}
                tagRenderer={this.renderSelectedTag}
                tagInputProps={{ tagProps: getTagProps, onRemove: this.handleTagRemove, rightElement: clearButton }}
                selectedItems={this.state.tags}
            />
        );
    }

    private renderSelectedTag = (tag: ImageTag) => <Tag
        round={true}
        key={`${tag.category}:${tag.value}`}
        style={{backgroundColor: this.props.categoryColor[tag.category]}}
    >
        {tag.value}
    </Tag>

    // NOTE: not using Tags.itemRenderer here so we can set icons.
    private renderTag: ItemRenderer<ImageTag> = (tag, { modifiers, handleClick }) => {
        if (!modifiers.matchesPredicate) {
            return null;
        }
        const text = `${tag.category}:${tag.value}`;
        return (
            <MenuItem
                active={modifiers.active}
                icon={this.isTagSelected(tag) ? "tick" : "blank"}
                label={tag.category}
                key={text}
                onClick={handleClick}
                text={tag.value}
                shouldDismissPopover={false}
            />
        );
    };

    private handleTagRemove = (_tag: string, index: number) => {
        this.deselectTag(index);
    };

    private getSelectedTagIndex(tag: ImageTag) {
        return this.state.tags.indexOf(tag);
    }

    private isTagSelected(tag: ImageTag) {
        return this.getSelectedTagIndex(tag) !== -1;
    }

    private selectTag(tag: ImageTag) {
        this.selectTags([tag]);
    }

    private selectTags(tagsToSelect: ImageTag[]) {
        const { tags, items } = this.state;

        let nextTags = tags.slice();
        let nextItems = items.slice();

        tagsToSelect.forEach(tag => {
            const results = maybeAddCreatedTagToArrays(nextItems, tag);
            nextItems = results.items;
            nextTags = !arrayContainsTag(nextTags, tag) ? [...nextTags, tag] : nextTags;
        });

        this.props.onSetSelectedItems(nextTags);

        this.setState({
            tags: nextTags,
            items: nextItems,
        });
    }

    private deselectTag(index: number) {
        let { tags } = this.state;
        tags = tags.filter((_tag, i) => i !== index);
        this.props.onSetSelectedItems(tags);
        this.setState({tags});
    }

    private handleTagSelect = (tag: ImageTag) => {
        if (this.props.items.indexOf(tag) == -1) {
            this.props.createItem(tag);
        }
        if (!this.isTagSelected(tag)) {
            this.selectTag(tag);
        } else {
            this.deselectTag(this.getSelectedTagIndex(tag));
        }
    };

    private handleTagsPaste = (tags: ImageTag[]) => {
        // On paste, don't bother with deselecting already selected values, just
        // add the new ones.
        this.selectTags(tags);
    };

    private handleClear = () => {
        this.props.onSetSelectedItems([]);
        this.setState({ tags: [] });
    }
}
