/*
 * Copyright 2019 Alexandre Guinaudeau. All rights reserved.
 */
import React, { Component } from 'react';
import _ from 'lodash';
import { PhotoSelector, PhotoStatus } from './photo_selection/photoSelector';
// import { PhotoMetadata } from './photo_selection/photoMetadata';
import { ExifPhoto } from './photo_selection/exifPhoto';
import { Photo, PhotoService, ImageFeatures, imageFeatures, ImageTag } from './../api/PhotoService';
import { Spinner, HotkeysTarget, Hotkeys, Hotkey, Card, Elevation, Dialog, Button, Intent, IconName, Icon, Menu, Navbar, Popover, Position, Checkbox, Alignment, MenuItem, Slider, Toaster, IToastProps } from '@blueprintjs/core';
import { SKELETON } from '@blueprintjs/core/lib/esm/common/classes';
import classNames from 'classnames';
import { Header } from './header';
import { __values } from 'tslib';
import { withRouter, Prompt } from 'react-router';
import { AlbumActions } from './album_selection/albumActions';
import { TagManager } from './tag_selection/tagManager';
import { MultiSelectTag } from './tag_selection/tagSelector';
import { arrayContainsTag, areTagsEqual } from './tag_selection/tags';
import { AlbumExportManager } from './photo_selection/albumExportManager';

export interface AlbumProps {
    match: any;
    location: any;
    history: any;
    album: string;
    photoService: PhotoService;
}

export interface TimeSort {
    type: "time";
}
export interface SimilaritySort {
    type: "similarity";
    photo: Photo;
}

export interface AlbumState {
    isLoading: boolean;
    isLoadingFeatures: boolean;
    isSaved: boolean;
    photos: Photo[];
    selected: number;
    selectTagInput: boolean;
    showHotkeys: boolean;
    isStatusVisible: { [key: string]: boolean };
    sort: TimeSort | SimilaritySort;
    features: { [key: string]: boolean };
    numSuggestedPhotos: number;
    manageTags: boolean;
    tags: ImageTag[];
    selectedTags: ImageTag[];
    categoryColor: { [category: string]: string };
    exportAlbum: boolean;
}

@HotkeysTarget
class UnconnectedAlbum extends Component<AlbumProps, AlbumState> {
    private toaster: Toaster;
    private refHandlers = {
        toaster: (ref: Toaster) => (this.toaster = ref),
    };

    constructor(props: AlbumProps) {
        super(props);

        this.state = {
            isLoading: true,
            isLoadingFeatures: true,
            isSaved: true,
            photos: [],
            selected: 0,
            selectTagInput: false,
            showHotkeys: false,
            isStatusVisible: this.getArrayMap(Object.keys(PhotoStatus), _ => true),
            sort: {
                type: "time"
            },
            features: this.getArrayMap(imageFeatures, key => key == "histogram_hue_saturation"),
            numSuggestedPhotos: 10,
            manageTags: false,
            tags: [],
            selectedTags: [],
            categoryColor: {},
            exportAlbum: false
        }
        this.state.isStatusVisible[PhotoStatus.IGNORED] = false;

        props.photoService.getPhotos(props.album).then(
            (photos: Photo[]) => this.setState({
                isLoading: false,
                photos: photos,
                tags: _.sortBy(_.uniqBy(
                    _.flatten(photos.map(photo => photo.tags || [])),
                    tag => `${tag.category}:${tag.value}`
                ), tag => `${tag.category}:${tag.value}`
                ),
            })
        ).catch(
            (e: any) => {
                this.addToast({
                    icon: "warning-sign",
                    intent: Intent.DANGER,
                    message: `Error loading images: ${e}`,
                })
            }
        );
    }

    componentDidUpdate = () => {
        if (!this.state.isSaved) {
          window.onbeforeunload = () => true
        } else {
          window.onbeforeunload = undefined
        }
    }

    render() {
        const { isLoading, photos, selected, selectTagInput, showHotkeys, sort, manageTags, tags, isSaved, exportAlbum } = this.state;

        if (isLoading) {
            return this.renderLoading();
        }

        const photo: Photo | undefined = photos[selected];

        return (
            <div>
                <Prompt
                    when={!isSaved}
                    message="Some changes ar not saved. Are you sure you want to leave?"
                />
                <Toaster ref={this.refHandlers.toaster} />
                <Dialog isOpen={showHotkeys} onClose={this.toggleHotkeys}>
                    {this.renderHotkeys()}
                </Dialog>
                <Dialog isOpen={manageTags} onClose={this.onToggleManageTags}>
                    <TagManager
                        tags={this.state.tags}
                        categoryColor={this.state.categoryColor}
                        onDeleteTag={this.onDeleteTag}
                        onEditTag={this.onEditTag}
                        onSetCategoryColor={this.onSetCategoryColor}
                    />
                </Dialog>
                <Dialog isOpen={exportAlbum} onClose={this.onToggleExportAlbum}>
                    <AlbumExportManager
                        photos={photos.filter(this.isPhotoVisible)}
                        album={this.props.album}
                        photoService={this.props.photoService}
                    />
                </Dialog>
                <Header 
                    elements={this.renderHeaderElements()}
                />
                <div className={"container main-photo"}>
                    <div className={"container inner"}>
                        <Button
                            minimal={true}
                            large={true}
                            icon={"document-open"}
                            onClick={this.openPhotoInFilesystem}
                            className={"fixed-top-right"}
                        />
                        {
                            photo && <ExifPhoto photo={photo}/>
                        }
                    </div>
                </div>
                <div className={"container left-sidebar"}>
                    <div className={"sort-button"}>
                        <Button 
                            icon={"sort"}
                            text={`Sorted by ${sort.type}`}
                            onClick={this.toggleSort}
                            fill={true}
                            intent={Intent.NONE}
                            alignText={Alignment.LEFT}
                        />
                    </div>
                    <PhotoSelector 
                        photos={photos}
                        tags={tags}
                        visible={photos.map(this.isPhotoVisible)}
                        selected={selected}
                        selectTagInput={selectTagInput}
                        sort={sort}
                        categoryColor={this.state.categoryColor}
                        onSelectPhoto={this.onSelectPhoto}
                        onChangeStatus={this.onChangeStatus}
                        getStatusIcon={this.getStatusIcon}
                        onCreateTag={this.onCreateTag}
                        onSetPhotoTags={this.onSetPhotoTags}
                    />
                </div>
                {/* <div className={"container right-sidebar"}>
                    {
                        photo && <PhotoMetadata photo={photo}/>
                    }
                </div> */}
            </div>
        );
    }

    public renderHotkeys() {
        return <Hotkeys>
            <Hotkey
                global={true}
                combo="up"
                label="Previous photo"
                onKeyDown={this.onPressUp}
                group={"Navigation"}
                preventDefault={true}
            />
            <Hotkey
                global={true}
                combo="down"
                label="Next photo"
                onKeyDown={this.onPressDown}
                group={"Navigation"}
                preventDefault={true}
            />
            <Hotkey
                global={true}
                combo="space"
                label="Toggle sort type"
                onKeyDown={this.toggleSort}
                group={"Navigation"}
                preventDefault={true}
            />
            <Hotkey
                global={true}
                combo="ctrl+s"
                label="Save"
                onKeyDown={this.saveStatuses}
                group={"Navigation"}
                preventDefault={true}
            />
            <Hotkey
                global={true}
                combo="left"
                label="Ignore photo"
                onKeyDown={this.onPressLeft}
                group={"Selection"}
                preventDefault={true}
            />
            <Hotkey
                global={true}
                combo="right"
                label="Select photo"
                onKeyDown={this.onPressRight}
                group={"Selection"}
                preventDefault={true}
            />
            <Hotkey
                global={true}
                combo="shift+left"
                label="Mark photo as default"
                onKeyDown={this.onPressShiftLeft}
                group={"Selection"}
                preventDefault={true}
            />
            <Hotkey
                global={true}
                combo="shift+right"
                label="Mark photo as suggested"
                onKeyDown={this.onPressShiftRight}
                group={"Selection"}
                preventDefault={true}
            />
            <Hotkey
                global={true}
                combo="tab"
                label="Focus on tag input"
                onKeyDown={this.onPressTab}
                group={"Tags"}
                preventDefault={true}
            />
        </Hotkeys>;
    }

    private addToast(toast: IToastProps) {
        toast.timeout = 5000;
        this.toaster.show(toast);
    }

    private onPressUp = () => {
        const { selected, photos } = this.state;
        for (let i = selected - 1; i >= 0; i--) {
            if (this.isPhotoVisible(photos[i])) {
                this.onSelectPhoto(i);
                return;
            }
        }
    }

    private onPressDown = () => {
        const { selected, photos } = this.state;
        for (let i = selected + 1; i < photos.length; i++) {
            if (this.isPhotoVisible(photos[i])) {
                this.onSelectPhoto(i);
                return;
            }
        }
        if (!this.isPhotoVisible(photos[selected])) {
            // The last photo has just been marked as invisible, go back up
            this.onPressUp();
        }
    }

    private onPressRight = () => {
        this.onChangeStatus(PhotoStatus.SELECTED);
    }

    private onPressLeft = () => {
        this.onChangeStatus(PhotoStatus.IGNORED);
    }

    private onPressShiftRight = () => {
        this.onChangeStatus(PhotoStatus.SUGGESTED);
    }

    private onPressShiftLeft = () => {
        this.onChangeStatus(PhotoStatus.DEFAULT);
    }

    private onPressTab = () => {
        // this.onAddTags(this.state.selectedTags);
        this.setState(prevState => ({ selectTagInput: !prevState.selectTagInput }));
    }

    private toggleHotkeys = () => {
        this.setState((prevState: AlbumState) => ({showHotkeys: !prevState.showHotkeys}));
    }

    private onSelectPhoto = (selected: number) => {
        this.setState({ selected, selectTagInput: false });
    }

    private onLoadFeatures = () => {
        this.props.photoService.getPhotoFeatures(this.props.album).then(
            (photosWithFeatures: Photo[]) => this.setState(prevState => {
                const photoFeatureMap: {[key: string]: ImageFeatures} = {};
                photosWithFeatures.forEach(p => {
                    photoFeatureMap[p.name] = p.features;
                });
                return {
                    isLoadingFeatures: false,
                    photos: prevState.photos.map(p => ({...p, features: photoFeatureMap[p.name]}))
                }
            })
        ).catch(
            (e: any) => {
                this.addToast({
                    icon: "warning-sign",
                    intent: Intent.DANGER,
                    message: `Error loading image features: ${e}`,
                })
            }
        );
    }

    private renderLoading = () => {
        const actions = {
            "left": [
                <AlbumActions
                    key={"album-actions"}
                    album={this.props.album}
                    photoService={this.props.photoService}
                    onRenameAlbum={this.onRenameAlbum}
                />
            ]
        }
        return (
            <div>
                <Header elements={actions}/>
                <div className={"container main-photo"}>
                    <Spinner className={"center-spinner"} size={Spinner.SIZE_LARGE}/>
                </div>
                <div className={"container left-sidebar"}>
                    {
                        Array.from({length: 10}, (_, i) => 
                            <Card 
                                className={classNames("img-card")}
                                elevation={Elevation.ONE}
                                key={i}
                            >
                                <span className={SKELETON}>Lorem ipsum dolor sit amet</span>
                                <span className={SKELETON}>consectetur adipiscing elit</span>
                            </Card>
                        )
                        
                    }
                </div>
                {/* <div className={classNames("container", "right-sidebar", SKELETON)} /> */}
            </div>
        );
    }

    private renderHeaderElements = (): { [alignment: string]: JSX.Element[] } => {
        const photoCounts = this.getArrayMap(Object.keys(PhotoStatus), _ => 0);
        this.state.photos.forEach(
            photo => {
                photoCounts[photo.status]++
            }
        )

        return {
            "left": [
                <AlbumActions
                    key={"album-actions"}
                    album={this.props.album}
                    photoService={this.props.photoService}
                    onRenameAlbum={this.onRenameAlbum}
                />,
                <Popover key={"options"} content={this.renderHeaderOptions()} position={Position.BOTTOM_LEFT}>
                    <Button icon="cog" minimal={true} text={"Settings"}/>
                </Popover>
            ],
            "right": [
                <MultiSelectTag
                    items={this.state.tags}
                    categoryColor={this.state.categoryColor}
                    selectedItems={this.state.selectedTags}
                    allowCreate={false}
                    onSetSelectedItems={this.onSetFilterTags}
                    className={"select-tag-header"}
                    placeholder={"Filter by Tag..."}
                    key={"filter-tags"}
                />,
                ...Object.keys(PhotoStatus).map(status => 
                    <Button 
                        icon={this.getStatusIcon(status)} 
                        text={photoCounts[status]} 
                        minimal={!photoCounts[status] || !this.state.isStatusVisible[status]}
                        disabled={!photoCounts[status]}
                        key={status}
                        onClick={() => this.toggleFilterStatus(status)}
                    />
                ),
                <Navbar.Divider key={"divider1"}/>,
                <Button 
                    icon={"floppy-disk"} 
                    minimal={true}
                    key={"save"}
                    disabled={this.state.isSaved}
                    onClick={this.saveStatuses}
                    intent={Intent.SUCCESS}
                />,
                <Button 
                    icon={"automatic-updates"} 
                    minimal={true}
                    key={"suggest"}
                    onClick={this.suggestPhotos}
                    intent={Intent.PRIMARY}
                />,
                <Navbar.Divider key={"divider2"}/>,
                <Button 
                    icon={"export"}
                    text={"Export"}
                    minimal={true}
                    key={"export"}
                    onClick={this.onToggleExportAlbum}
                    intent={Intent.SUCCESS}
                />,
            ]
        }
    }

    private renderHeaderOptions = () => {
        const numPhotos = this.state.photos.filter(p => p.status != "IGNORED").length
        let labelStepSize = Math.pow(10, Math.floor(Math.log10(numPhotos)));
        if (numPhotos / labelStepSize > 4) {
            labelStepSize *= 2
        } else if (numPhotos / labelStepSize < 2) {
            labelStepSize /= 2
        }
        let maybeRenderFeatureOptions: JSX.Element;

        if (this.state.isLoadingFeatures) {
            maybeRenderFeatureOptions = <MenuItem
                icon={"predictive-analysis"} 
                onClick={this.onLoadFeatures} 
                text={"Load image features"}
                key={"features"}
            />
        } else {
            maybeRenderFeatureOptions = <div>
                <MenuItem text={"Number of selected images"}>
                    <Slider
                        max={numPhotos - 1}
                        labelStepSize={labelStepSize}
                        value={_.min([numPhotos-1, this.state.numSuggestedPhotos])}
                        onChange={this.onChangeNumSuggestedPhotos}
                    />
                </MenuItem>
                <MenuItem text={"Image features"}>
                { 
                    imageFeatures.map(feature => 
                    <Checkbox 
                        checked={this.state.features[feature]}
                        key={feature}
                        label={feature}
                        onChange={() => this.toggleFeature(feature as keyof ImageFeatures)}
                    />
                    )
                }
                </MenuItem>
            </div>
        }

        return <Menu>
            <MenuItem
                icon={"key-command"} 
                onClick={this.toggleHotkeys} 
                text={"Hotkeys"}
                key={"hotkey"}
            />
            <Button 
                icon={"tag"}
                key={"tag"}
                minimal={true}
                text={"Manage Tags"}
                onClick={this.onToggleManageTags}
            />
            {maybeRenderFeatureOptions}
        </Menu>
    }

    private saveStatuses = () => {
        this.props.photoService.save(this.props.album, this.state.photos).then(
            () => this.setState(
                {isSaved: true},
                () => {
                    this.addToast({
                        icon: "tick",
                        intent: Intent.SUCCESS,
                        message: `Succssfully saved statuses`
                    })
                }
            )
        ).catch(
            (e: any) => {
                this.addToast({
                    icon: "warning-sign",
                    intent: Intent.DANGER,
                    message: `Error saving status: ${e}`,
                })
            }
        );
        (document.activeElement as HTMLElement).blur();
    }

    private suggestPhotos = () => {
        this.props.photoService.suggestPhotos(this.props.album, this.state.photos, this.state.numSuggestedPhotos).then(
            photos => this.setState({ photos })
        ).catch(
            (e: any) => {
                this.addToast({
                    icon: "warning-sign",
                    intent: Intent.DANGER,
                    message: `Error suggesting photos: ${e}`,
                })
            }
        );
        (document.activeElement as HTMLElement).blur();
    }

    private toggleFeature = (feature: keyof ImageFeatures) => {
        this.setState(prevState => ({
            features: {
                ...prevState.features,
                [feature]: !prevState.features[feature]
            }
        }))
    }

    private toggleSort = () => {
        if (this.state.isLoadingFeatures) {
            console.warn("Image Features have not been loaded yet");
            return;
        }
        const photoRef = this.state.photos[this.state.selected];
        this.setState(prevState => {
            let sort: TimeSort | SimilaritySort;
            let photos: Photo[];
            if (prevState.sort.type == "similarity") {
                sort = { type: "time" };
                photos = prevState.photos.map(photo => ({
                    ...photo, 
                    ["distance"]: undefined
                }));
                return {
                    photos: photos.sort(this.sortTime),
                    sort,
                    selected: 0
                }
            } else {
                const photoRef = prevState.photos[prevState.selected];
                const featureRef = this.getFeatures(photoRef);
                sort = { type: "similarity", photo: photoRef };
                photos = prevState.photos.map(photo => ({
                    ...photo, 
                    ["distance"]: _.sum(featureRef.map((value, index) => Math.abs(value - this.getFeatures(photo)[index])))
                }));
                return {
                    photos: photos.sort((p1, p2) => (p1.distance - p2.distance)),
                    sort,
                    selected: 0
                }
            }
        }, () => this.onSelectPhoto(this.state.photos.map(p => p.name).indexOf(photoRef.name)));
    }

    private sortTime = (a: Photo, b: Photo): number => {
        if (a.date.isValid() && b.date.isValid()) {
            const diff: number = a.date.unix() - b.date.unix()
            if (diff != 0) {
                return diff
            }
        }
        if (!a.date.isValid() && b.date.isValid()) {
            return 1
        }
        if (a.date.isValid() && !b.date.isValid()) {
            return -1
        }
        return a.name > b.name ? 1 : -1;
    }

    private getFeatures(photo: Photo) {
        if (photo === undefined) {
            return [];
        }
        return _.flatten(imageFeatures.map(
            feature => this.state.features[feature] ? (photo.features as any)[feature] : []
        ))
    }

    private toggleFilterStatus = (status: PhotoStatus) => {
        this.setState(prevState => ({
            isStatusVisible: {...prevState.isStatusVisible, [status]: !prevState.isStatusVisible[status]}
        }));
        (document.activeElement as HTMLElement).blur();
    }

    private getStatusIcon(status: PhotoStatus) {
        let intent: Intent;
        let icon: IconName;
        switch (status) {
            case "IGNORED":
                intent = Intent.DANGER;
                icon = "disable";
                break;
            case "DEFAULT":
                icon="edit";
                intent=Intent.NONE;
                break;
            case "SUGGESTED":
                icon="endorsed";
                intent=Intent.PRIMARY;
                break;
            case "SELECTED":
                icon="confirm";
                intent=Intent.SUCCESS;
                break;
            default:
                icon="help";
                intent=Intent.WARNING;
                break;
        }
        return (
            <Icon icon={icon} intent={intent}/>
        );
    }

    private isPhotoVisible = (photo: Photo) => {
        const { isStatusVisible, selectedTags } = this.state
        if (!isStatusVisible[photo.status]) {
            return false;
        }
        const relevantTags = (photo.tags || []).filter(t => arrayContainsTag(selectedTags, t));
        const selectedCategories = _.uniq(selectedTags.map(tag => tag.category));
        const matchedCategories = _.uniq(relevantTags.map(tag => tag.category));
        const missingCategories = _.difference(selectedCategories, matchedCategories);
        return missingCategories.length == 0;
    }

    private onChangeStatus = (status: PhotoStatus) => {
        this.setState(prevState => {
            prevState.photos[prevState.selected].status = status;
            return {...prevState, isSaved: false}
        }, this.onPressDown)
    }

    private onSetPhotoTags = (tags: ImageTag[]) => {
        this.setState(prevState => {
            prevState.photos[prevState.selected].tags = tags;
            return {...prevState, isSaved: false}
        });
    }

    private onSetFilterTags = (selectedTags: ImageTag[]) => {
        this.setState({selectedTags});
    }

    private onChangeNumSuggestedPhotos = (value: number) => {
        this.setState({numSuggestedPhotos: _.max([1, value])});
    }

    private onRenameAlbum = (name: string) => {
        this.props.photoService.renameAlbum(name, this.props.album).then(() => {
            this.props.history.push(`/${encodeURIComponent(name)}`);
        }).catch(
            (e: any) => {
                this.addToast({
                    icon: "warning-sign",
                    intent: Intent.DANGER,
                    message: `Error renaming album: ${e}`,
                })
            }
        );
    }

    private openPhotoInFilesystem = () => {
        this.props.photoService.open(this.props.album, this.state.photos[this.state.selected].name);
    }

    private onToggleManageTags = () => {
        this.setState((prevState: AlbumState) => ({manageTags: !prevState.manageTags}));
    }

    private onCreateTag = (tag: ImageTag) => {
        if (tag.value) {
            this.setState((prevState: AlbumState) => ({
                tags: _.sortBy([...prevState.tags, tag], ["category", "value"])
            }));
        }
    }

    private onEditTag = (oldTag: ImageTag, newTag: ImageTag) => {
        this.setState((prevState: AlbumState) => ({
            tags: prevState.tags.map(t => areTagsEqual(t, oldTag) ? newTag : t),
            selectedTags: prevState.selectedTags.map(t => areTagsEqual(t, oldTag) ? newTag : t),
            photos: prevState.photos.map(p => {
                p.tags = (p.tags || []).map(t => areTagsEqual(t, oldTag) ? newTag : t);
                return p;
            }),
            isSaved: false
        }));
    }

    private onDeleteTag = (tag: ImageTag) => {
        this.setState((prevState: AlbumState) => ({
            tags: prevState.tags.filter(t => t != tag),
            selectedTags: prevState.selectedTags.filter(t => t != tag),
            photos: prevState.photos.map(p => {
                p.tags = (p.tags || []).filter(t => !areTagsEqual(t, tag));
                return p;
            }),
            isSaved: false
        }));
    }

    // private onSelectTag = (tag: ImageTag) => {
    //     this.setState((prevState: AlbumState) => {
    //         const { selectedTags } = prevState;
    //         const index = selectedTags.indexOf(tag);
    //         if (index >= 0) {
    //             selectedTags.splice(index, 1);
                
    //             return { selectedTags };
    //         }
    //         return {
    //             selectedTags: [...selectedTags, tag]
    //         }
    //     });
    // }

    private onSetCategoryColor = (category: string, color: string) => {
        this.setState(prevState => {
            prevState.categoryColor[category] = color;
            return prevState;
        })
    }

    private onToggleExportAlbum = () => {
        this.setState((prevState: AlbumState) => ({exportAlbum: !prevState.exportAlbum}));
    }

    private getArrayMap(array: string[], fun: (key: string) => any): { [key: string]: any } {
        return array.reduce(
            (obj, key) => ({ ...obj, [key]: fun(key)}), {}
        );
    } 
}

export const Album = withRouter(UnconnectedAlbum);
