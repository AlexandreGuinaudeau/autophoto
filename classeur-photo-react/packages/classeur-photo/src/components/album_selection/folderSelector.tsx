/*
 * Copyright 2019 Alexandre Guinaudeau. All rights reserved.
 */
import React, { Component } from 'react';
import { PhotoService, ImageFolder, ListDirResult } from '../../api/PhotoService';
import { Menu, Spinner, Button, Intent } from '@blueprintjs/core';
import _ from 'lodash';

export interface FolderSelectorProps {
    photoService: PhotoService
    onSelectFolder: (folder: string, album: string) => void;
}

export interface FolderSelectorState {
    separator: string,
    children: ImageFolder[];
    current?: ImageFolder;
    isLoading: boolean;
}

export class FolderSelector extends Component<FolderSelectorProps, FolderSelectorState> {
    public state: FolderSelectorState = {
        separator: "",
        children: [],
        current: undefined,
        isLoading: true,
    }

    public componentWillMount() {
        this.props.photoService.getHomeDir().then((json) => this.setState({current: {folder: json.home, num_images: 0, is_exact: true}, separator: json.separator}));
    }

    public componentWillUpdate(_newProps: FolderSelectorProps, newState: FolderSelectorState) {
        const current = this.state.current || {} as any;
        const newCurrent = newState.current || {} as any;
        if (newCurrent.folder && current.folder != newCurrent.folder) {
            this.props.photoService.listDirs(newCurrent.folder).then((json: ListDirResult) => {
                this.setState({
                    children: json.children, 
                    current: json.current,
                    isLoading: false
                });
            });
        }
    }

    public render() {
        const { separator, children, current, isLoading } = this.state;
        if (!current) {
            return <Spinner size={Spinner.SIZE_LARGE}/>
        }
        return (
            <Menu className={"relative-position"}>
                <h3>
                    {current.folder}
                    <span className={"align-right"}>
                        {current.is_exact ? "" : "~"}{current.num_images} images
                    </span>
                </h3>
                <Menu.Divider />
                {
                    current.folder.split(separator).length > 0 ?
                    <div className={"relative-position"}>
                        <Menu.Item 
                            icon={"undo"} 
                            text={"Parent Folder"}
                            onClick={() => this.setState(prevState => ({
                                current: {
                                    ...prevState.current,
                                    folder: prevState.current.folder.split(separator).length == 2 ?
                                        "/" : prevState.current.folder.split(separator).slice(0,-1).join(separator)
                                }, 
                                children: [], 
                                isLoading: true
                            }))}
                        />
                        <Menu.Divider />
                    </div> :
                    null
                }
                <div className={"subfolders relative-position"}>
                {
                    isLoading ? <Spinner size={Spinner.SIZE_LARGE} /> : children.map(this.renderFolder)
                }
                </div>
                <Button 
                    icon={"add"} 
                    text={`Select ${this.getFolderName(current.folder, separator)}`}
                    fill={true}
                    intent={Intent.SUCCESS}
                    onClick={this.createAlbum}
                />
            </Menu>
        );
    }

    private renderFolder = (imageFolder: ImageFolder) => {
        return (
        <Menu.Item 
            icon={"folder-open"} 
            key={imageFolder.folder} 
            text={
            <div>
                {imageFolder.folder}
                <span className={"align-right"}>{`${imageFolder.is_exact ? "" : "~"}${imageFolder.num_images} images`}</span>
            </div>
            }
            onClick={() => this.setState(prevState => ({
                current: {
                    folder: `${prevState.current.folder}${prevState.separator}${imageFolder.folder}`, 
                    num_images: imageFolder.num_images,
                    is_exact: imageFolder.is_exact
                },
                children: [], 
                isLoading: true
            }))}
        />);
    }

    private getFolderName = (path: string, separator: string) => {
        return path.slice(path.lastIndexOf(separator) + 1);
    }

    private createAlbum = () => {
        const { current, separator } = this.state;
        const album = this.getFolderName(current.folder, separator);
        this.props.onSelectFolder(current.folder, album);
    }
}