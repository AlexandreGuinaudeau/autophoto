/*
 * Copyright 2019 Alexandre Guinaudeau. All rights reserved.
 */
import React, { Component } from 'react';
import { PhotoService } from '../../api/PhotoService';
import { FolderSelector } from './folderSelector';
import { Dialog, Spinner, NonIdealState, Menu, Button, Intent, ButtonGroup, Alert } from '@blueprintjs/core';

export interface AlbumSelectorProps {
    photoService: PhotoService
    onSelectAlbum: (album: string) => void;
    onDeleteAlbum: (album: string) => Promise<void>;
}

export interface AlbumSelectorState {
    albums: string[];
    ready: boolean;
    isDialogOpen: boolean;
    albumToDelete: string | null;
}

export class AlbumSelector extends Component<AlbumSelectorProps, AlbumSelectorState> {
    public state: AlbumSelectorState = {
        albums: [],
        ready: false,
        isDialogOpen: false,
        albumToDelete: null
    };

    constructor(props: AlbumSelectorProps) {
        super(props);
        this.props.photoService.getAlbums().then(
            albums => this.setState({albums, ready: true})
        )
    }

    public render() {
        const { albums, ready, isDialogOpen, albumToDelete } = this.state;
        const { photoService } = this.props;
        if (!ready) {
            return <Spinner size={Spinner.SIZE_LARGE}/>
        }

        return (
            <div>
                <Alert
                    cancelButtonText="Cancel"
                    confirmButtonText={`Delete ${albumToDelete}`}
                    icon="trash"
                    intent={Intent.DANGER}
                    isOpen={albumToDelete != null}
                    onCancel={this.onCancel}
                    onClose={this.onCancel}
                    onConfirm={this.onDeleteAlbum}
                >
                    <p>
                        Are you sure you want to delete <b>{albumToDelete}</b>?
                        You will be not able to restore it later.
                    </p>
                </Alert>
                <div className={"album-selector"}>
                {albums.length ?
                <Menu className={"album-menu"}>{albums.map(this.renderAlbum)}</Menu> :
                <NonIdealState title={"There is no album yet"}/>}
                <Button
                    fill={true}
                    icon={"add"} 
                    intent={Intent.SUCCESS}
                    onClick={this.renderNewAlbum}
                >Create new album</Button>
                <Dialog isOpen={isDialogOpen} onClose={this.onDialogClose}>
                    <FolderSelector photoService={photoService} onSelectFolder={this.onSelectFolder}/>
                </Dialog>
            </div>
            </div>
        );
    }

    private onSelectFolder = (folder: string, album: string) => {
        this.props.photoService.createAlbum(folder, album).then(() => {
            this.props.onSelectAlbum(album);
        });
    }

    private renderAlbum = (album: string) => {
        return <ButtonGroup key={album} minimal={true} fill={true}>
            <Button
                icon={"folder-close"}
                text={album}
                onClick={() => this.props.onSelectAlbum(album)}
                className={"display-inline-block"}
            />
            <Button
                icon={"trash"}
                intent={Intent.DANGER}
                className={"flex-unset"}
                onClick={() => this.setState({albumToDelete: album})}
            />
        </ButtonGroup>
    }

    private renderNewAlbum = () => {
        this.setState({isDialogOpen: true}); 
    }

    private onDialogClose = () => {
        this.setState({isDialogOpen: false}); 
    }

    private onCancel = () => {
        this.setState({albumToDelete: null});
    }

    private onDeleteAlbum = () => {
        const { albumToDelete } = this.state;
        this.props.onDeleteAlbum(albumToDelete).then(
            () => this.setState(prevState => ({
                albums: prevState.albums.filter(album => album != albumToDelete),
                albumToDelete: null
            }))
        );
    }
}