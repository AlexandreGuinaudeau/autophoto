/*
 * Copyright 2019 Alexandre Guinaudeau. All rights reserved.
 */
import { Component, ChangeEvent } from "react";
import { Popover, Position, Button, Menu, MenuItem, MenuDivider, Intent, Alert, Dialog } from '@blueprintjs/core';
import React from 'react';
import { PhotoService } from '../../api/PhotoService';
import { withRouter } from 'react-router';
import { FolderSelector } from './folderSelector';

export interface AlbumActionsProps {
    match: any;
    location: any;
    history: any;
    album: string;
    photoService: PhotoService;
    onRenameAlbum: (name: string) => void;
}

export interface AlbumActionsState {
    isRenamingAlbum: boolean;
    isDeletingAlbum: boolean;
    isEnrichingAlbum: boolean;
    album: string;
}

class UnconnectedAlbumActions extends Component<AlbumActionsProps, AlbumActionsState> {
    constructor (props: AlbumActionsProps) {
        super(props);

        this.state = {
            isRenamingAlbum: false,
            isDeletingAlbum: false,
            isEnrichingAlbum: false,
            album: props.album,
        }
    }

    render() {
        const { isDeletingAlbum, isRenamingAlbum, isEnrichingAlbum } = this.state;
        return <div>
            <Popover content={this.renderAlbumOptions()} position={Position.BOTTOM_LEFT}>
                <Button icon="folder-open" text={this.props.album} minimal={true}/>
            </Popover>
            <Dialog title="Rename Album" isOpen={isRenamingAlbum} onClose={this.onCancel}>
                <input
                    type="text"
                    className="bp3-input"
                    placeholder="Enter a new name for the album..."
                    value={this.state.album}
                    onChange={this.onChangeValue}
                />
                <Button intent={Intent.SUCCESS} text={`Rename to ${this.state.album}`} onClick={this.onRenameAlbum}/>
            </Dialog>
            <Dialog isOpen={isEnrichingAlbum} onClose={this.onCancel}>
                <FolderSelector
                    photoService={this.props.photoService}
                    onSelectFolder={this.onSelectFolder}
                />
            </Dialog>
            <Alert
                cancelButtonText="Cancel"
                confirmButtonText={`Delete ${this.props.album}`}
                icon="trash"
                intent={Intent.DANGER}
                isOpen={isDeletingAlbum}
                onCancel={this.onCancel}
                onClose={this.onCancel}
                onConfirm={this.onDeleteAlbum}
            >
                <p>
                    Are you sure you want to delete <b>{this.props.album}</b>?
                    You will be not able to restore it later.
                </p>
            </Alert>
        </div>
    }

    private renderAlbumOptions = () => {
        return <Menu>
            <MenuItem text={"Rename album"} icon={"edit"} onClick={this.onClickRename}/>
            <MenuItem text={"Add photos to album"} icon={"add"} onClick={this.onClickAdd}/>
            <MenuItem text={"Open album in file system"} icon={"folder-shared-open"} onClick={this.openAlbumInFilesystem}/>
            <MenuDivider />
            <MenuItem text={"Delete album"} icon={"trash"} intent={Intent.DANGER} onClick={this.onClickDelete}/>
        </Menu>
    }

    private onClickRename = () => {
        this.setState({isRenamingAlbum: true});
    }

    private onRenameAlbum = () => {
        this.props.onRenameAlbum(this.state.album);
        this.setState({isRenamingAlbum: false});
    }

    private onChangeValue = (event: ChangeEvent<HTMLInputElement>) => {
        this.setState({album: event.currentTarget.value});
    }

    private onClickAdd = () => {
        this.setState({isEnrichingAlbum: true});
    }

    private onSelectFolder = (folder: string) => {
        this.props.photoService.enrichAlbum(folder, this.state.album).then(() => {
            window.location.reload();
        });
    }

    private onClickDelete = () => {
        this.setState({isDeletingAlbum: true});
    }

    private onCancel = () => {
        this.setState({isDeletingAlbum: false, isRenamingAlbum: false, isEnrichingAlbum: false});
    }

    private onDeleteAlbum = () => {
        this.props.photoService.deleteAlbum(this.props.album).then(() => {
            this.props.history.push("/");
        });
    }

    private openAlbumInFilesystem = () => {
        this.props.photoService.open(this.props.album, ".");
    }
}

export const AlbumActions = withRouter(UnconnectedAlbumActions);