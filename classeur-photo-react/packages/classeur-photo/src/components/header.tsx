/*
 * Copyright 2019 Alexandre Guinaudeau. All rights reserved.
 */
import { Alignment, Button, Navbar } from "@blueprintjs/core";
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

export interface HeaderProps {
    match: any;
    location: any;
    history: any;
    elements?: { [alignment: string]: JSX.Element[] };
}

class UnconnectedHeader extends Component<HeaderProps> {
    public render() {
        const { elements } = this.props;
        return (
            <div className={"container header"}>
                <Navbar>
                    <Navbar.Group align={Alignment.LEFT}>
                        <Navbar.Heading>Autophoto</Navbar.Heading>
                        <Navbar.Divider />
                        <Button 
                            minimal={true}
                            icon="home" 
                            text="Home" 
                            onClick={this.onClickHome} 
                        />
                        { elements && elements[Alignment.LEFT] || null }
                    </Navbar.Group>
                    <Navbar.Group align={Alignment.RIGHT}>
                        { elements && elements[Alignment.RIGHT] || null }
                    </Navbar.Group>
                </Navbar>
            </div>
        );
    }

    private onClickHome = () => {
        this.props.history.push("/")
    }
}

export const Header = withRouter(UnconnectedHeader);
