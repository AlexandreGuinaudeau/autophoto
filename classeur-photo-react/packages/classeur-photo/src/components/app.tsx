/*
 * Copyright 2019 Alexandre Guinaudeau. All rights reserved.
 */
import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import _ from 'lodash';
import { Header } from './header';
import { AlbumSelector } from './album_selection/albumSelector';
import { PhotoService } from './../api/PhotoService';
import { Album } from './album';

export class App extends Component {
    private photoService: PhotoService;

    constructor(props: any) {
        super(props);

        this.photoService = new PhotoService();
    }

    render() {
        return (
            <Router>
                <Route exact path="/" render={this.renderHome} />
                <Route path="/" component={this.renderAlbums} />
            </Router>
        );
    }

    public renderHome = ({ history }: any) => (
        <div>
            <Header />
            <div className={"container album-selection"}>
                <AlbumSelector 
                    photoService={this.photoService} 
                    onSelectAlbum={(album: string) => { history.push(`/${encodeURIComponent(album)}`) }}
                    onDeleteAlbum={this.onDeleteAlbum}
                />
            </div>
        </div>
    );

    public renderAlbums = () => (
        <Route path={`/:album`} component={this.renderAlbum} />
    );

    public renderAlbum = ({ match }: any) => (
        <Album photoService={this.photoService} album={decodeURIComponent(match.params.album)} />
    );

    private onDeleteAlbum = (album: string) => {
        return this.photoService.deleteAlbum(album);
    }
}

export default App;