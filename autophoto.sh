#!/bin/bash

SERVER_PID_FILE=var/run/server.pid
CLIENT_PID_FILE=var/run/client.pid
SERVER_LOG_FILE=var/log/server.log
CLIENT_LOG_FILE=var/log/client.log

start () {
    if [[ ($1 == "server")]]; then
        python3 -u autophoto/main.py &> var/log/server.log &
        echo $! > $SERVER_PID_FILE;
    elif [[ ($1 == "client")]]; then
        yarn --cwd classeur-photo-react dev &> var/log/client.log &
        echo $! > $CLIENT_PID_FILE;
        sleep 5;
        xdg-open http://localhost:9000/;
    elif [[ ($1 == "both")]]; then
        start "server";
        start "client";
    else
        echo "Unexpected argument: $1";
    fi
}

stop () {
    if [[ ($1 == "server")]]; then
        if ! [ -f $SERVER_PID_FILE ]; then
            echo "Server is not running";
            exit 1;
        fi
        server_pid=$(cat $SERVER_PID_FILE);
        kill $server_pid;
        rm $SERVER_PID_FILE;
    elif [[ ($1 == "client")]]; then
        if ! [ -f $CLIENT_PID_FILE ]; then
            echo "Client is not running";
            exit 1;
        fi
        client_pid=$(cat $CLIENT_PID_FILE);
        kill $client_pid;
        node_pid=$(lsof -i | grep 9000 | grep LISTEN | awk '{print $2}')
        kill $node_pid;
        rm $CLIENT_PID_FILE;
    elif [[ ($1 == "both")]]; then
        stop "server";
        stop "client";
    else
        echo "Unexpected argument: $1";
    fi
}

status () {
    if [[ ($1 == "server")]]; then
        if [ -f $SERVER_PID_FILE ]; then
            server_pid=$(cat $SERVER_PID_FILE);
            if ps -p $server_pid > /dev/null; then
                echo "Running"
                exit 0;
            else
                echo "Process dead but PID file found"
                exit 1;
            fi
        else
            echo "Stopped"
            exit 0;
        fi
    elif [[ ($1 == "client")]]; then
        if [ -f $CLIENT_PID_FILE ]; then
            client_pid=$(cat $CLIENT_PID_FILE);
            if ps -p $client_pid > /dev/null; then
                echo "Running"
                exit 0;
            else
                echo "Process dead but PID file found"
                exit 1;
            fi
        else
            echo "Stopped"
            exit 0;
        fi
    elif [[ ($1 == "both")]]; then
        SERVER_STATUS=$(status server)
        CLIENT_STATUS=$(status client)
        if [ "$SERVER_STATUS" = "Running" ] && [ "$CLIENT_STATUS" = "Running" ]; then
            echo "Running"
            exit 0
        elif [ "$SERVER_STATUS" = "Stopped" ] && [ "$CLIENT_STATUS" = "Stopped" ]; then
            echo "Stopped"
            exit 0
        else
            echo "Process dead but PID file found"
            exit 1
        fi
    else
        echo "Unexpected argument: $1";
    fi
}

clean () {
    if [[ ($1 == "server")]]; then
        rm $SERVER_LOG_FILE;
    elif [[ ($1 == "client")]]; then
        rm $CLIENT_LOG_FILE;
    elif [[ ($1 == "both")]]; then
        clean "server";
        clean "client";
    else
        echo "Unexpected argument: $1";
    fi
}

ACTION=$1
KIND=${2:-both}
STATUS=$(status $KIND)

if [[ ($ACTION == "start")]]; then
    if [ "$STATUS" = "Running" ]; then
        echo "Autophoto is already running";
        exit 0;
    elif [ "$STATUS" = "Undefined" ]; then
        stop "both";
    fi
    start $KIND && echo "Started"
elif [[ ($ACTION == "stop")]]; then
    if [ "$STATUS" = "Stopped" ]; then
        echo "Autophoto is already stopped";
        exit 0;
    fi
    stop $KIND && echo "Stopped"
elif [[ ($ACTION == "status")]]; then
    echo $STATUS
elif [[ ($ACTION == "clean")]]; then
    clean $KIND && echo "Cleaned"
else
    echo "Usage: ./autophoto.sh (start|stop|status|clean)";
fi